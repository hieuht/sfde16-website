#!/usr/bin/env python
#coding:utf-8

import urllib, urllib2
import re
import pandas as pd
import StringIO
from bs4 import BeautifulSoup as bs

url = 'http://vietnam.in2p3.fr/2016/star/star_participants.php'
# Get content
req = urllib2.Request(url)
res = urllib2.urlopen(req)
html = res.read()
#print html
re_html = re.search(r'<table  class="table_participants">(.*)</table>', html, re.I|re.S)
table = re_html.group(0)
# Parse rows
soup = bs(table)
trs = soup.findAll("tr")
# From website
unsorted_list = []
for tr in trs[1:]:    
    row = []
    for td in tr.findAll("td"):
        row.append(td.text.strip())
    print row[5]
    # firstname, lastname, institution = row[2], row[1], row[3]
    # row = [firstname, lastname, institution]
    # unsorted_list.append(row)
