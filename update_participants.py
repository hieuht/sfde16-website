#!/usr/bin/env python
#coding:utf-8

import urllib, urllib2
import re
import pandas as pd
import StringIO
from bs4 import BeautifulSoup as bs

url = 'http://vietnam.in2p3.fr/2016/star/star_participants.php'
# Get content
req = urllib2.Request(url)
res = urllib2.urlopen(req)
html = res.read()
#print html
re_html = re.search(r'<table  class="table_participants">(.*)</table>', html, re.I|re.S)
table = re_html.group(0)
# Parse rows
soup = bs(table)
trs = soup.findAll("tr")
# From website
unsorted_list = []
for tr in trs[1:]:    
    row = []
    for td in tr.findAll("td"):
        row.append(td.text.strip())
    firstname, lastname, institution = row[2], row[1], row[3]
    row = [firstname, lastname, institution]
    unsorted_list.append(row)

# Append new item
# Firstname, Lastname, insitution
row = []

# Wrong format
a="""Priya Hasan|Maulana Azad National Urdu University
Steve Longmore|LJMU
Som Nath|Bose National Centre for Basic Sciences
Gemma Busquet|Institut de Ciències de l’Espai, CSIC-IEEC
Matthias González|U. Paris Diderot - CEA, France
Tom Megeath|University of Toledo and the Herschel Orion Protostar Team
Maulana Azad|National Urdu University, Hyderabad, India
O. Nayak|Johns Hopkins U.
Basmah Riaz|MPE
Alessio Traficante|IAPS-INAF
Cecile Favre|Institut de Planetologie et d'Astrophysique de Grenoble, IPAG, France
Alberto Sanna|MPIfR
Yuan Wang|Max-Planck Institute for Astronomy
Eun-Jung Chung|KASI
Jessy Jose|Kavli Institute for Astronomy and Astrophysics
Yuji Ebisawa|University of Tokyo
Bringfried Stecklum|Thueringer Landessternwarte
Stefanie Walch|I. Physics Institute, University of Cologne
Daniel Seifried|University of Cologne
Shu-ichiro Inutsuka|Nagoya University
Torsten Stamer|Nagoya University
Hans Nguyen|MpIfR
Michael Mattern|MpifR
Ngoc Nguyen-Thi-Bich|Tay Nguyen University
Suri Sumeyye|University of Cologne
Gwendoline Stefan|University of Cologne
Yoka Okada|University of Cologne
Gemma Busquet|CSIC-IEEC
James Chibueze|University of Nigeria
Uehara Kenta|University of Tokyo
Michael Chen|University of Victoria
Andres Escala|University of Chile
Paola Caselli|Max-Planck-Institute for Extraterrestrial Physics 
Thomas Stanke|ESO"""
for iii in a.split('\n'):
	name, institution = iii.split('|')
	lastname, firstname = name.split(' ')
	row.append([firstname, lastname, institution])

# Right format
b="""Thushara Pillai|Mpifr
Tsuyoshi Inoue|NAOJ
Christopher Matzner|University of Toronto
Tomofumi Umemoto|NAOJ
Yuri Nishimura|University of Tokyo
Kengo Tachihara|Nagoya University
Mikito Kohno|Nagoya University
Tokuda kazuki|Osaka Prefecture University"""
for yyy in b.split('\n'):
	name, institution = yyy.split('|')
	firstname, lastname = name.split(' ')
	row.append([firstname, lastname, institution])

# Uncommon format
row.append(['Huynh Anh', 'Le-Nguyen', 'Kyung Hee University'])
row.append(['Myung Gyoon', 'Lee', 'Seoul National University Dept of Physics and Astronomy'])
unsorted_list += row 

# Create table and sorted and uniq by firstname and lastname   
sorted_list = sorted(unsorted_list, key = lambda x: str(x[0]+' '+x[1]))
pd_list = pd.DataFrame(sorted_list, columns =['firstname', 'lastname', 'institution'])
uniq_list = pd_list.drop_duplicates(['firstname', 'lastname'], keep='last')
for index, row in uniq_list.iterrows():
	firstname = row[0]
	lastname = row[1]
	institution = row[2]
	print "\t\t<tr><td width='5%%'>%s</td><td>%s %s</td><td>%s</td></tr>" % (index+1, firstname, lastname, institution)